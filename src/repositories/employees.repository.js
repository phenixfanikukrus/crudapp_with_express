const path = require('path');
const uuid = require('uuid/v1');
const { readJsonFile, writeJsonFile } = require('../utils/file.utils');
const { isDuplicate } = require('../utils/validation');
const FILE_PATH = path.resolve(path.dirname(require.main.filename), '..', 'data', 'employees.json');

const usersRepository = {

    async getAll() {
        return await readJsonFile(FILE_PATH) || [];
    },

    async get(id) {
        const employees = await this.getAll();
        return employees.find(employee => employee.id === id);
    },

    async add(employee) {
        const employees = await this.getAll();
        const newEmployee = {
            id: uuid(),
            ...employee
        };

        return new Promise(async (resolve, reject) => {
            if(!isDuplicate(newEmployee.name,newEmployee.surname,newEmployee.age,newEmployee.department,employees)){
                employees.push(newEmployee);
                // console.log(this.get(newEmployee.id));
                await writeJsonFile(FILE_PATH, employees);
                resolve(newEmployee);
            } else {
                reject(new Error(`Employee already exists`));
            }
        })
    },

    async delete(id) {
        const employees = await this.getAll();

        const index = employees.findIndex(employee => employee.id === id);
        if (index !== -1) {
            employees.splice(index, 1);
            await writeJsonFile(FILE_PATH,employees);
        }
    },
    async edit(id, body) {
        const employees = await this.getAll();
        const index = employees.findIndex(item => item.id === id);
        employees[index].name = body.name || employees[index].name;
        employees[index].surname = body.surname || employees[index].surname;
        employees[index].gender = body.gender || employees[index].gender;
        employees[index].age = body.age || employees[index].age;
        employees[index].department = body.department || employees[index].department;   
        await writeJsonFile(FILE_PATH, employees);
    }
};

module.exports = usersRepository;
