const deleteButtons = document.querySelectorAll('.action__jsDelete');
const editButtons = document.querySelectorAll('.action__jsEdit');
    
for (let el of deleteButtons) {
    el.addEventListener('click', async e => {
        e.preventDefault();
        await fetch(`/employees/delete/${el.getAttribute('data-id')}`, 
        {
            method: "DELETE"
        });
    })
}

for (let el of editButtons) {
    el.addEventListener('click', e => {
        e.preventDefault();
        window.location.href = `/employees/edit/${el.getAttribute('data-id')}`;
    })
}