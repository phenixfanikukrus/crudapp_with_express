const jsName = document.querySelector('.jsName');
const jsSurname = document.querySelector('.jsSurname');
const jsGender = document.querySelector('.jsGender');
const jsAge = document.querySelector('.jsAge');
const jsDepartment = document.querySelector('.jsDepartment');
const jsForm = document.querySelector('.jsForm');
const jsWarning = document.querySelector('.jsWarning');

jsForm.addEventListener('submit', e => {
    let warnings = [];
    const name = jsName.value.trim();
    jsName.value = name;
    const surname = jsSurname.value.trim();
    jsSurname.value = surname;
    const gender = jsGender.value.trim();
    jsGender.value = gender;
    const age = jsAge.value.trim();
    jsAge.value = age;
    const department = jsDepartment.value.trim();
    jsDepartment.value = department;

    if (name === '' || name == null) {
        warnings.push('Name is required');
    }

    if (surname === '' || surname == null) {
        warnings.push('Surname is required');
    }
    if (gender === '' || gender == null) {
        warnings.push('Gender is required');
    }
    if (age === '' || age == null) {
        warnings.push('Age is required');
    }
    if (department === '' || department == null) {
        warnings.push('Department is required');
    }

    if (warnings.length > 0) {
        e.preventDefault();
        jsWarning.style.display = 'block';
        jsWarning.innerHTML = warnings.join('<br>');
    }
})