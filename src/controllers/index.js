const { Router } = require('express');
const homeController = require('./home.controller');
const employeesController = require('./employees.controller');
// const apiControllers = require('./api');

const router = new Router();

router.use('/', homeController);
router.use('/employees', employeesController);
// router.use('/api', apiControllers);

module.exports = router;