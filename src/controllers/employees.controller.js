const { Router } = require('express');
const employeesRepository = require('../repositories/employees.repository');

const router = new Router();

router.get('/', async (_request, response) => {
    const employees = await employeesRepository.getAll();
    response.render('pages/employees/view', { employees });
});

router.get('/add', (_request, response) => {
    response.render('pages/employees/add');
});

router.get('/edit/:id', async (request, response) => {
    const employee = await employeesRepository.get(request.params.id);
    if (employee) {
        response.render('pages/employees/edit', {employee});
    } 
});

router.get('/', async (_request, response) => {
    const employees = await employeesRepository.getAll();
    response.json(employees);
});

router.get('/:id', async(request, response) => {
    const employee = await employeesRepository.get(request.params.id);
    if (employee) {
        response.json(employee);
        return;
    }
    response.status(404);
});

router.post('/add', async (request, response) => {
    try {
        const employee = await employeesRepository.add(request.body);
        response.redirect('/employees').status(201).json({
            success: true,
            data: employee
    });
    } catch (e) {
        response.status(500).json({
            success: false,
            error: e.message
        });
    }
});

router.delete('/delete/:id', async (request, response) => {
    await employeesRepository.delete(request.params.id); 
    response.status(200).json({
        success: true
    });
});

router.post('/edit/:id',async (request, response) => {
    try {
        await employeesRepository.edit(request.params.id, request.body) 
        response.redirect('/employees').status(200).json({
            success: true
        });
    } catch (e) {
        response.status(500).json({
            success: false,
            error: e.message
        });
    }
});



module.exports = router;